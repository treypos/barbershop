# Приложение Barbershop (Ruby(Sinatra)).

## Описание:
Для работы формы требуется установить и подключить гемы:

bash:
```bash
gem install sinatra
gem install sqlite3
```
в app.rb:
```ruby
require 'sinatra'
require 'sqlite3'
```

## Запуск приложения:

1. Перейти в каталог app

bash:
```bash
cd app/
```

2. Запустить приложение:

bash:
```bash
ruby app.rb
```

3. Открыть в браузере http://localhost:4567/